
# 百百堡垒机(英文名称:baibai) 基于web的远程控制，无任何插件,远程就是这么简单。
# :pray: :pray: :pray: 撸码不易，请点个星星，在此谢过！ :pray: :pray: :pray: 

此分支是 2.0.0 版本，主要面向单个团队、单个企业用户，账号由管理员统一分配，被控方无权限拒绝发起的远程控制。2.0.0 跟 1.x.x 版本对比，有着全新的管理界面、全新的远程控制、全新的客户端授权机制，能够给你带来极致的体验。

# 在线体验

http://82.157.157.202:8080/

账号：baibai

密码:123456


## 打个广告
市面上有很多HTTP模拟请求工具，有yapi，有ApiPost，好不好用在此不做评价。这里推荐使用```BB-API```,功能全面，免费使用、资源无限制，自动生成接口文档，界面简洁大气。支持局域网部署，打造属于你、公司的HTTP 协议管理工具。
官网地址:http://yun-api.com/
## 打个广告结束



``` 友情提示：在体验过程如有问题请加群讨论，不要恶意修改数据，谢谢。 ```

客户端下载地址，请下载最新的２.X.X版本:https://gitee.com/baibaiclouds/prober/releases/

``` 注意：由于百百客户端未进行数字证书认证，在运行客户端前，请关闭杀毒软件或加入杀毒软件白名单。 ```

运行客户端成功之后，修改客户端的平台地址，把IP改成82.157.157.202,端口不用修改。

客户端改完IP稍等片刻就会自动连接上平台

# :pray: :pray: :pray: 撸码不易，请点个星星，在此谢过！ :pray: :pray: :pray: 

# :one:核心功能有以下
|功能名称 |功能描述|
|-----|-----|
|web远程|基于 web 的远程控制，无任何插件|
|远程协议|协议支持 vnc，rdp，ssh|
|文件发送|支持发送文件，控制者和被控制者能够相互发送文件。|
|录像审计|在远程控制时，能够实时录像，方便后续审计使用|
|无公网控制|配合[客户端](https://gitee.com/baibaiclouds/prober)使用即使被控 PC 没有公网也能进行远程控制,随时随地进行远程控制。|
|账号权限|支持个人、企业级的权限控制。账号由管理员进行统一分配、回收。|
|其它功能|还有更多功能进入系统慢慢去体检吧 :no_mouth: :no_mouth: :no_mouth:  |

# :two:[官网地址 http://bb.app-yun.com/](http://bb.app-yun.com/)

### 部署系统

```在点个星星之后再加群即可获得详细部署资料```

QQ群:1053592770

![qq](https://img-blog.csdnimg.cn/20200726232850251.png)

# 开发计划

|功能名称 |开发状态|版本 |
|-----|-----|-----|
|基础功能|已支持|1.0.2|
|客户端支持windows,linux|已支持|1.0.6|
|传文件|已支持|1.2.0|
|远程控制录屏、播放录屏|已支持|1.3.0|
|专业版本(面向单个团队的运维，全新的系统、控制界面)|已支持|2.0.0|
|第三方账号认证、接入|正在开发|2.1.0|

# :three:系统整体技术栈

java-jdk1.8

平台-spring boot+vue+ElementUI+guacamole

客户端-electron

远程控制-vnc ultra

内网穿透-frp-有进行增强，主要增加端口验证，预防恶意连接、代理。[frp 源码](https://github.com/baibaicloud/frp)

数据库-mysql

平台程序支持运行在 linux

客户端支持 windows64 位、linux64。

# :four:插个私人广告 BB-API HTTP 请求工具

致力于打造简洁、免费、好用的 HTTP 模拟请求工具，自动生成接口文档。

帮助您公司、团队、个人提高开发效率。

官网地址：http://yun-api.com

# :five:系统相关界面
- 系统初始化1

![1](https://img-blog.csdnimg.cn/77f132b8cc7345de8b124bfcb761e71f.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA5rOV5Y-377ya5ZCD6IKJ,size_20,color_FFFFFF,t_70,g_se,x_16)

- 系统初始化2

![2](https://img-blog.csdnimg.cn/2d29d2c34e5f48a5a82e2be7fdb3aa2b.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA5rOV5Y-377ya5ZCD6IKJ,size_20,color_FFFFFF,t_70,g_se,x_16)

- 首页，显示系统概况，无聊时还可以玩玩```小恐龙```游戏

![3](https://img-blog.csdnimg.cn/c96787a9e8204b2195eab649a0f53502.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA5rOV5Y-377ya5ZCD6IKJ,size_20,color_FFFFFF,t_70,g_se,x_16)

- 准备发起远程控制

![4](https://img-blog.csdnimg.cn/0676a88f6d964f17b0ae510e1a04beaa.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA5rOV5Y-377ya5ZCD6IKJ,size_20,color_FFFFFF,t_70,g_se,x_16)

- 准备发起远程控制，选择画面清晰度，根据网络情况进行选择。

![5](https://img-blog.csdnimg.cn/e8cee240861e4d748effc58167dfc759.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA5rOV5Y-377ya5ZCD6IKJ,size_20,color_FFFFFF,t_70,g_se,x_16)

- 客户端界面，正在远程。如果把文件拖入客户端界面就可```轻松传文件给WEB端```。

![6](https://img-blog.csdnimg.cn/07ddd1db71004797ac0603e44d8a0b59.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA5rOV5Y-377ya5ZCD6IKJ,size_20,color_FFFFFF,t_70,g_se,x_16)

- WEB端正在远程控制

![7](https://img-blog.csdnimg.cn/4a0b246830654b99a3660ee30cc5a692.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA5rOV5Y-377ya5ZCD6IKJ,size_20,color_FFFFFF,t_70,g_se,x_16)

- 远程关机

![8](https://img-blog.csdnimg.cn/95e4e095b0f6437ca1ebe69b7b5f619f.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA5rOV5Y-377ya5ZCD6IKJ,size_20,color_FFFFFF,t_70,g_se,x_16)

- 权限管理，能够控制各个账号权限

![9](https://img-blog.csdnimg.cn/4a3517c029e046909952287902b97773.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA5rOV5Y-377ya5ZCD6IKJ,size_20,color_FFFFFF,t_70,g_se,x_16)

- 审计，远程时能够实时录像，方便后续审计使用

![10](https://img-blog.csdnimg.cn/aa8c6d66514d4b77a122c1c531468609.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA5rOV5Y-377ya5ZCD6IKJ,size_20,color_FFFFFF,t_70,g_se,x_16)

- 还有更多的功能进入系统体验吧