/**
 * $Revision$
 * $Date$
 *
 * Copyright (C) https://gitee.com/baibaiclouds/platform loon. All rights reserved.
 * <p>
 * This software is the confidential and proprietary information of loon.
 * You shall not disclose such Confidential Information and shall use it only
 * in accordance with the terms of the agreements you entered into with loon.
 * 
 * Modified history:
 *   Loon  2019年11月2日 下午11:58:03  created
 */
package com.desktop.web.controller.web;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.desktop.web.core.exception.BusinessException;
import com.desktop.web.core.utils.RequestUtil;
import com.desktop.web.core.utils.Util;
import com.desktop.web.core.web.Result;
import com.desktop.web.service.config.ConfigService;
import com.desktop.web.service.role.RoleService;
import com.desktop.web.service.user.UserService;
import com.desktop.web.service.websession.WebSession;
import com.desktop.web.service.websession.WebSessionService;
import com.desktop.web.uda.entity.User;

/**
 * 
 *
 * @author baibai
 */
@Controller
public class WebSessionController extends BaseWebController {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Value("${frp.last.date.max.sec}")
    private Integer LAST_DATE_MAX_SEC;

    @Autowired
    private WebSessionService webSessionService;

    @Autowired
    private ConfigService configService;

    @Autowired
    private UserService userService;

    @Autowired
    private RoleService roleService;

    @RequestMapping(value = "/webapi/websession/login", method = {RequestMethod.POST})
    @ResponseBody
    public Object login(HttpServletRequest request) {

        try {

            Map<String, String> params = RequestUtil.getBody(request);
            if (params == null || params.isEmpty()) {
                throw new BusinessException("参数为空");
            }

            Map<String, Object> retData = new HashMap<String, Object>();
            WebSession webSession = webSessionService.webLogin(params.get("username"), params.get("password"));
            retData.put("token", webSession.getToken());
            retData.putAll(this.makeSessionInfo(webSession.getUid()));
            return Result.Success(retData);
        } catch (BusinessException e) {
            logger.error(e.getMessage(), e);
            return Result.Error(e.getMessage());
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return Result.Error();
        }
    }

    @RequestMapping(value = "/webapi/websession/session/get", method = {RequestMethod.GET})
    @ResponseBody
    public Object getSession(HttpServletRequest request) {

        try {
            Long uid = RequestUtil.getUid();
            return Result.Success(this.makeSessionInfo(uid));
        } catch (BusinessException e) {
            logger.error(e.getMessage(), e);
            return Result.Error(e.getMessage());
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return Result.Error();
        }
    }

    private Map<String, Object> makeSessionInfo(Long uid) {
        Map<String, Object> retData = new HashMap<String, Object>();
        retData.put("admin_root_uid", configService.getAdminRootUID());
        retData.put("unknow_node_id", configService.getUnknowNodeId());
        retData.put("last_date_max_sec", LAST_DATE_MAX_SEC);

        User user = userService.getUserById(uid);
        retData.put("selfname", user.getSelfname());
        retData.put("teamName", configService.getTeamName());

        List<String> rights = roleService.getRightTargetListByUid(uid);
        retData.put("rights", Util.listToString(rights));
        return retData;
    }

}
