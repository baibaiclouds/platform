/**
 * 
 */
package com.desktop.web.uda.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.desktop.web.core.db.BaseEntity;

/**
 * @author baibai
 *
 */
@TableName("t_right")
public class Right extends BaseEntity {
	
    @TableField(exist = false)
    private static final long serialVersionUID = 1L;

    private String title;
    private String target;

    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title the title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return the target
     */
    public String getTarget() {
        return target;
    }

    /**
     * @param target the target to set
     */
    public void setTarget(String target) {
        this.target = target;
    }

}
