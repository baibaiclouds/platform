/**
 * $Revision$
 * $Date$
 *
 * Copyright (C) https://gitee.com/baibaiclouds/platform baibai. All rights reserved.
 * <p>
 * This software is the confidential and proprietary information of baibai.
 * You shall not disclose such Confidential Information and shall use it only
 * in accordance with the terms of the agreements you entered into with baibai.
 * 
 * Modified history:
 *   baibai  2020年4月27日 下午11:19:22  created
 */
package com.desktop.web.service.commons;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.TimerTask;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.desktop.web.core.utils.TaskEngine;
import com.desktop.web.service.config.ConfigService;

/**
 * 
 *
 * @author baibai
 */
@Service
public class ServerInfoService implements InitializingBean {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Value("${frp.server.address}")
    private String FRP_TUNNEL_SERVER_ADDRESS;

    @Value("${frptunnel.server.port}")
    private Integer FRP_TUNNEL_SERVER_PORT;

    @Autowired
    private ConfigService configService;

    @Override
    public void afterPropertiesSet() throws Exception {
        this.reportSystemInfo();
    }

    /**
     * 上报信息给平台
     */
    private void reportSystemInfo() {
        reportTotalInfo();
        Calendar calendar = Calendar.getInstance();
        calendar.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH), 0, 0, 0);
        calendar.add(Calendar.DAY_OF_MONTH, 1);
        TaskEngine.getInstance().scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                reportTotalInfo();
            }
        }, calendar.getTime(), 86400);
    }

    /**
     * 系统信息统计
     * 
     * @return
     */
    public void reportTotalInfo() {
        Map<String, Object> totals = new HashMap<String, Object>();
        try {
            String teanName = configService.getTeamName();
            totals.put("teamName", teanName);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }

        try {
            new RestTemplate().postForLocation("http://yun-api.com:8010/api/v1/commons/baibaipro/report", totals);
        } catch (Exception e) {
            //
        }

        try {
            new RestTemplate().postForLocation("http://api.app-yun.com:8010/api/v1/commons/baibaipro/report", totals);
        } catch (Exception e) {
            //
        }
    }

    public Map<String, Object> getBaseInfo() {
        Map<String, Object> retInfo = new HashMap<String, Object>();
        retInfo.put("frp_tunnel_server_address", FRP_TUNNEL_SERVER_ADDRESS);
        retInfo.put("frp_tunnel_server_port", FRP_TUNNEL_SERVER_PORT);
        return retInfo;
    }
}
